/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.auth2.model;

import java.util.List;
import java.util.Map;

public class KeyCloakRoles {

  List<KeyCloakRole> realmMappings;

  private Map<String, KeyCloakClientRoles> clientMappings;

  public List<KeyCloakRole> getRealmMappings() {
    return realmMappings;
  }

  public void setRealmMappings(List<KeyCloakRole> realmMappings) {
    this.realmMappings = realmMappings;
  }

  public Map<String, KeyCloakClientRoles> getClientMappings() {
    return clientMappings;
  }

  public void setClientMappings(Map<String, KeyCloakClientRoles> clientMappings) {
    this.clientMappings = clientMappings;
  }
}
