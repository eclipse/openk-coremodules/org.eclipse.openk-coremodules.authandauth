////
******************************************************************************
* Copyright © 2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
*
*     http://www.eclipse.org/legal/epl-v10.html
*
******************************************************************************
////
= openKonsequenz - How to deploy the module "portal@openK" (Auth&Auth)
:Author: Frank Dietrich, Simon Reis
:Date: 2017-11-17
:Revision: 2
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark
:icons: font
:imagesdir: ../images

<<<

== Requirements
* Apache Tomcat 8.0.30 (up to 8.5, confirmed)
* Keycloak Server Standalone server distribution (up to version 16 major, confirmed)


=== Install Apache Tomcat
Tomcat is an open-source Java Servlet Container and provides a "pure Java" HTTP web server environment in which Java code can run.

* Download Tomcat version 8.0.30 from https://archive.apache.org/dist/tomcat/tomcat-8/v8.0.30/bin/
and extract it (apache-tomcat-8-0-30.zip).


== Deployment Auth&Auth

The Auth&Auth application consists of 3 parts regarding the deployment.

. Frontend: "portalFE"-Directory
. Backend: "portal.war"
. Keycloak: OAuth2 and JWT Service implementation

=== Deployment of the application components

==== Deployment of the frontend

The Frontend SPA is built as a folder, that contains all the required files for
the application. If you have built the frontend yourself then the content of the "dist"-folder
within the Frontend development-directory has to be copied into the
target-directory of the apache-tomcat. If you got the Frontend delivered in form of a archive (portalFE.zip) put *its content*
into the target-directory "portalFE" of the apache-tomcat:

 <Apache-Tomcat>/webapps/portalFE

If the target folder does not exist, it has to be created manually.

==== Deployment of the backend
The backend REST-services are built in form of a "WAR"-file "portal.war" (to be found
in the "target"-directory of the MAVEN-directory structure or delivered to you).
For deployment, this file has to be copied to the directory

 <Apache-Tomcat>/webapps

==== Deployment of Keycloak


. Get "Keycloak 7.0.0" (08.06.2022: working with 16.1.1) from the Keycloak download page. +
Download "Standalone server distribution" (Win: zip / Linux: tar.gz) https://www.keycloak.org/downloads.html

. Extract the content.

. This step is for older versions of Keycloak, with newer versions of Keycloak you can create the initial admin account via UI (<<_configuration_of_keycloak>>).
To create an initial admin account for Keycloak run the script `add-user-keycloak.sh` with username 'admin'
and a strong password.

 <Keycloak-Directory>/bin/add-user-keycloak.sh -r master -u <username> -p <password>

. To start Keycloak create a *Linux-Service which executes* the `standalone.sh` script to be found in the Keycloak `bin`
directory.

 <Keycloak-Directory>/bin/standalone.sh


TIP: Keycloaks default port is 8080 you can change it to for example to port 8181 with the following
execution parameter: -Djboss.socket.binding.port-offset=1010

 <Keycloak-Directory>/bin/standalone.sh -Djboss.socket.binding.port-offset=1010

==== Configuration of the system

===== Configuration of the webserver

There exists the file *context.xml* in the "conf" subdirectory (*<TOMCAT>/conf*) of the target apache tomcat installation.
Add the following parameter and resource in order to access the correct backend configurations and to
gain access to the database (see "Configuration of the backend" below):

.context.xml
[source,xml]
----
[...]
    <!-- Uncomment this to disable session persistence across Tomcat restarts -->

    <!--Manager pathname=""/>

    <!-- Parameter for different Backend configs  -->
    <Parameter name="environment" override="false" value="Development"/-->

    <Parameter name="OK_PORTAL_ENVIRONMENT" override="false" value="Production"/>

    <!-- Uncomment this to enable Comet connection tacking (provides events
         on session expiration as well as webapp lifecycle) -->
[...]
----


===== Configuration of the backend

After the backend war file has been deployed and unpacked inside of the *<TOMCAT>/webapps* folder there are different
 backend config files to be found in the folder *<TOMCAT>/webapps/elogbook/WEB-INF/classes*

* backendConfig*.json
* moduleConfig*.json

The active configuration is chosen by parameter *OK_PORTAL_ENVIRONMENT* (see context.xml above).
Possible values are:

* *Custom* (for backendConfigCustom.json and moduleConfigCustom.json)
* *DevLocal* (for backendConfigDevLocal.json and moduleConfigDevLocal.json)
* *DevServer* (for backendConfigDevServer.json and moduleConfigDevServer.json)
* *Production* (for backendConfigProduction.json and moduleConfigProduction.json)

After choosing an environment the corresponding json files have to be configured:

.backendConfigXXX.json
[source,json]
----
{
  "internalSessionLengthMillis": 46800000,
  "reloadUsersInSec": 300,
  "authServerUrl": "http://localhost:8080/",
  "keycloakRealm": "OpenKRealm",
  "keycloakClient": "OpenKClient",
  "keycloakAdmin": "admin",
  "keycloakPW": "adminpwd",
  "maxLoadUsers": 1000
}
----
* *internalSessionLengthMillis* - Maximum allowed duration of inactivity, before a session ends
* *reloadUsersInSec* - Polling interval for getting all users from keycloak
* *authServerUrl* - Base url of the keycloak instance
* *keycloakRealm* - Configured realm on keycloak
* *keycloakClient* - Configured client on keycloak
* *keycloakAdmin* - user for admin access on keycloak
* *keycloakPW* - password of the admin user
* *maxLoadUsers* - Sets the maximum of users that are loaded from keycloak. KC itself is limited here. The value
should be set to the amount of *all* users in the target "keycloakRealm".


.moduleConfigXXX.json
[source,json]
----
[
  {
    "name": "Betriebstagebuch",
    "cols": 1,
    "rows": 1,
    "color": "#ffffff",
    "link": "http://172.18.22.160:8880/elogbookFE",
    "pictureLink": "https://www.openkonsequenz.de/.../m_logbuch_2443636.jpg",
    "requiredRole": "elogbook-access"

  },
  {
    "name": "Bereitschaftsplanung",
    "cols": 1,
    "rows": 1,
    "color": "#ffffff",
    "link": "https://www.openkonsequenz.de/..../94-bereitschaftsplan",
    "pictureLink": "https://www.openkonsequenz.de/medien/.../l_bereitschaftsplan_57882047.jpg",
    "requiredRole": "planning-access"
  },

  ...

]

----

* *name* - Name/description of the module
* *cols* - always "1"
* *rows* - always "1"
* *color* - always "#ffffff"
* *link* - URL for the module
* *pictureLink* - URL for picture belonging to the module
* *requiredRole* - Name of the role (on keycloak) the user must have to access the module


[#_configuration_of_keycloak]
===== Configuration of Keycloak



. Login in Keycloak

.. Go to http://<KEYCLOAK-SERVER-IP>:<CONFIGURED-KEYCLOAK-PORT>/ +
Hit the `Administration Console` -Link.Either login with admin account created during the Keycloak-deployment
(see above) or (with newer versions of Keycloak you can) create the initial admin account here.

. Add a new realm: `Elogbook`

.Add a new realm overview
[options="header,footer"]
image::newRealm1.png[new Realm]

TIP: With the "Import" option on the "Add realm" page you can import a preconfigured/exported Realm
so you don't have to do the steps 3-6 manually! This file can be found in the repo under
 _deploy\conf\preconfiguredRealms\preconfiguredRealmOpenkonsequenz_keycloak_16.1.1.json_ .
Continue with adding Users after the import -> Step 7

.Add a new realm
[options="header,footer"]
image::newRealm2.png[new Realm]

[start=3]
. Create a client with client ID `elogbook-backend`.

.Create a client overview
[options="header,footer"]
image::createClient.png[create Client]

.Add a client
[options="header,footer"]
image::addClient.png[add Client]

[start=4]
. Set the client settings accordingly:

.Client settings of elogbook-backend
[options="header,footer"]
image::clientSettings.png[link="images/clientSettings.png"]

Go to tab `Mappers`. `Create` all mappers like seen below (click for full size)

.Mappers of elogbook-backend
[option="header,footer"]
image::KeycloakMappersOverview.png[link="images/KeycloakMappersOverview.png"]

- family name
- given name
- username
- roles


and configure them accordingly:

.FamilyName mapper
[option="header,footer"]
image::KeycloakMapperFamilyName.png[Mapper settings]

.GivenName mapper
[option="header,footer"]
image::KeycloakMapperGivenName.png[Mapper settings]

.Username mapper
[option="header,footer"]
image::KeycloakMapperUsername.png[Mapper settings]

.Role mapper
[option="header,footer"]
image::KeycloakMapperRoles.png[Mapper settings]



[start=5]
. Add the roles under `Roles` press `Add Role`. Add the following Roles for your modules: +
For module: Elogbook (Betriebstagebuch):
 - elogbook-normaluser
 - elogbook-superuser
 - elogbook-access +

+
For Module: ContactBaseData (Kontaktstammdaten):
 - kon-access
 - kon-admin
 - kon-reader
 - kon-writer

.Roles (click for full size)
[option="header,footer"]
image::KeycloakRoles.png[link="images/KeycloakRoles.png"]

[start=6]
. Configuring a superuser role. Under Roles, hit on a superuser (elogbook-superuser or planned-policies-superuser) or press `Edit`
.. Switch `Composite Roles` to on.
.. Add the Realm Role 'elogbook-normaluser' or 'planned-policies-normaluser' correspondingly from the `Available Roles`
.. Select from the `Client Roles` Dropdown-List 'realm-management' and add the role 'view-users'.

.Elogbook superuser composite role
[options="header,footer"]
image::roleSuperUser.png[Elogbook superuser composite role]

[start=7]
. Adding users under `Users` press `Add User`.

.Add a user
[options="header,footer"]
image::addingUsers.png[Add a User]

.. Fill in username, First Name, Last Name, else is default and save it.
.. Go to the `Credentials` tab and type in a new password and the confirmation as well, disable `Temporary`. Press `Reset Password`.
Press `Change Password`.

.Credentials tab
[options="header,footer"]
image::credentials.png[Credentials tab]

[start=3]
.. Go to the `Role Mappings` tab. +
Here you grant rights/roles to the users:
Select the from the previously created roles in `Available Roles` and hit `Add selected`.
Add the roles to grant the user access/rights for the according modules you have installed. +

... User right in the distinct modul elogbook (`elogbook-access` required of course):
- elogbook-normaluser = standard user in the elogbook-app
- elogbook-superuser = admin user in the elogbook-app

... User right in the distinct modul Contact Base Data (`kon-access` required of course):
- kon-admin
- kon-reader
- kon-writer

... User right in the distinct modul Planned Grid Measures (`planned-policies-access` required of course):
- planned-policies-normaluser = standard user in the plannedGridMeasures-app
- planned-policies-superuser = admin user in the plannedGridMeasures-app

... Access rights to the specific modul:
- elogbook-access
- kon-access
- planning-access
- feedin-management-access
- planned-policies-access

.Role Mappings tab
[options="header,footer"]
image::roleMappings.png[Role Mapping]


[start=4]
.. Go back to the `Details` tab and make sure `Recquired User Actions` is empty. Delete anything which is in there with a press on the little cross.

[start=8]
. Add an admin user. (Mandatory) +
Same procedure like adding a User (see 7.) but with the role 'elogbook-superuser' or 'planned-policies-superuser' instead of
'elogbook-normaluser' or 'planned-policies-normaluser' correspondingly.

IMPORTANT: We use this admin account (username & password) also to configure the backend. +
See <<Configuration of the backend>>,
username & password from here is keycloakAdmin & keycloakPW there.
















