/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.portal.auth2.model.KeyCloakUser;
import java.util.List;
import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.eclipse.openk.portal.exceptions.PortalUnauthorized;
import org.eclipse.openk.portal.viewmodel.LoginCredentials;
import org.eclipse.openk.portal.viewmodel.UserAuthentication;
import org.eclipse.openk.portal.viewmodel.UserModule;
import org.eclipse.openk.portal.viewmodel.VersionInfo;
import org.junit.Ignore;
import org.junit.Test;

public class BackendControllerTest  extends ResourceLoaderBase {

    @Test
    public void testGetVersionInfo() {
        BackendController bec = new BackendController();
        VersionInfo vi = bec.getVersionInfo();

        String vCurr = getClass().getPackage().getImplementationVersion();
        assertEquals( vi.getBackendVersion(), vCurr );
    }

    @Test
    public void getUserModuleList_TemporaryOnly() {
        //_fd only temporarily
        List<UserModule> modList = new BackendController().getUserModuleList();
        assertTrue( !modList.isEmpty() );
    }

}
