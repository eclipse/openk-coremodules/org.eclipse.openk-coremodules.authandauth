/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;

import org.eclipse.openk.portal.auth2.model.JwtToken;
import org.eclipse.openk.portal.common.BackendConfig;
import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.eclipse.openk.portal.exceptions.PortalUnauthorized;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import java.util.HashMap;
import static org.junit.Assert.*;

public class TokenManagerTest extends ResourceLoaderBase {

    @Before
    public void cleanUp() {
        TokenManager sessione = TokenManager.getInstance();
        HashMap<String, TokenManager.SessionItem> regMap = Whitebox.getInternalState(sessione, "registry");
        regMap.clear();
    }

    @Test
    public void testRegisterNewSession() {
        final int COUNT = 1000;

        for (int i = 0; i < COUNT; i++) {
            TokenManager.getInstance().registerNewSession(createJWT( "T"+i ));
        }
    }


    @Test(expected = PortalUnauthorized.class)
    public void testRefreshStatusIsAlive() throws InterruptedException, PortalUnauthorized {
        TokenManager sessione = TokenManager.getInstance();
        JwtToken specialToken = createJWT(null);

        long timeNow = System.currentTimeMillis();
        sessione.registerNewSession(createJWT("HUGO1"));
        sessione.registerNewSession(specialToken);
        sessione.registerNewSession(createJWT("HUGO3"));

        HashMap<String, TokenManager.SessionItem> regMap = Whitebox.getInternalState(sessione, "registry");

        assertEquals(3, regMap.size());
        assertTrue(regMap.containsKey("HUGO1"));
        assertTrue(regMap.containsKey(specialToken.getAccessToken()));
        assertTrue(regMap.containsKey("HUGO3"));

        regMap.remove(specialToken.getAccessToken());
        regMap.put(specialToken.getAccessToken(), new TokenManager.SessionItem(timeNow - BackendConfig.getInstance().getInternalSessionLengthMillis() - 1));
        regMap.get(specialToken.getAccessToken()).setJwtToken(specialToken);
        regMap.remove("HUGO3");
        regMap.put("HUGO3", null);

        Thread.sleep(50); // NOSONAR // LOCK is held

        TokenManager.getInstance().refreshSessionIsAlive("HUGO1", true);

        // create Time stays the same
        TokenManager.SessionItem sit1 = regMap.get("HUGO1");
        assertTrue(sit1.getSessionLastAccessTime() > sit1.getSessionCreationTime());

        sit1.setSessionCreationTime(sit1.getSessionLastAccessTime()-200);
        assertEquals( sit1.getSessionCreationTime()+200, sit1.getSessionLastAccessTime());
        sit1.setUser(null);
        assertNull(sit1.getUser());
        assertEquals("HUGO1", sit1.getSessionId() );
        sit1.setCookieToken("CaptainCookie");
        assertEquals("CaptainCookie", sit1.getCookieToken());

        TokenManager.getInstance().logout("HUGO1");
        assertEquals(2, regMap.size());
        TokenManager.getInstance().logout( "FAKE");
        assertEquals(2, regMap.size());


        // session out of time
        TokenManager.getInstance().refreshSessionIsAlive(specialToken.getAccessToken(), true);
        // fails with PortalUnauthorized
    }

    private JwtToken createJWT(String accessToken ) {
        String json = super.loadStringFromResource("JWTAdmin.json");
        JwtToken jwt = JsonGeneratorBase.getGson().fromJson(json, JwtToken.class);

        if( accessToken != null ) {
            jwt.setAccessToken(accessToken);
        }
        return jwt;
    }


}
