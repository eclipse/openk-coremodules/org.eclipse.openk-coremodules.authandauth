/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.viewmodel;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserModuleTest {

    @Test
    public void testModuleConfigProduction() {
        UserModule[] um = UserModule.getInstance();
        assertEquals( "Betriebstagebuch local 4200", um[0].getModuleName() );
        assertEquals( "#ffffff", um[0].getColor() );
        assertEquals( 1, (long)um[0].getCols() );
        assertEquals( 1, (long)um[0].getRows() );
        assertEquals( "http://localhost:4200/", um[0].getLink() );
        assertEquals( "https://www.openkonsequenz.de/medien/cache/main_image/m_logbuch_2443636.jpg", um[0].getPictureLink() );
        assertEquals( "elogbook-access", um[0].getRequiredRole() );

        assertEquals( "moduleConfigDevLocal.json", UserModule.getConfigFileName());
        UserModule.setConfigFileName("moduleConfigDevLocal.json");
        assertEquals( "moduleConfigDevLocal.json", UserModule.getConfigFileName() );
    }

}
