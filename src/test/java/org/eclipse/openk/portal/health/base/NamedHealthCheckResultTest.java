/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
package org.eclipse.openk.portal.health.base;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NamedHealthCheckResultTest {
    @Test
    public void testAll() {
        NamedHealthCheckResult h1 = new NamedHealthCheckResult("Uno", Result.healthy());
        NamedHealthCheckResult h2 = new NamedHealthCheckResult("Due", Result.unhealthy("Darum"));

        assertEquals("\"Uno\":{\"healthy\":true}", h1.toJson());
        assertEquals("\"Due\":{\"healthy\":false,\"message\":\"Darum\"}", h2.toJson());

        List<NamedHealthCheckResult> lista = new LinkedList<>();
        lista.add(h1);
        lista.add(h2);
        assertEquals( "{\"Uno\":{\"healthy\":true},\"Due\":{\"healthy\":false,\"message\":\"Darum\"}}",
                NamedHealthCheckResult.toJson(lista));
    }
}
